
# coding: utf-8

# In[38]:

import numpy as np
import random
import itertools
import math


# In[132]:

from sklearn import linear_model, cross_validation, preprocessing
from scipy.stats import logistic


# In[2]:
import pdb
pdb.set_trace()
num_items = 10
num_users = 500
num_items_features = 20
num_users_features = 10


# In[3]:

items_features_cor = 0
users_features_cor = 0


# In[4]:

L_rank = 5


# In[5]:

S_sparsity = 0.05


# In[6]:

num_ele = num_items_features*(num_users_features+1)


# In[7]:

user_mu = np.zeros((num_users_features))


# In[8]:

user_sigma = np.eye(num_users_features)


# In[9]:

user_input = np.random.multivariate_normal(user_mu, user_sigma, num_users)


# In[10]:

item_mu = np.zeros((num_items_features))


# In[11]:

item_sigma = np.eye(num_items_features)


# In[12]:

item_input = np.random.multivariate_normal(item_mu, item_sigma, num_items)


# In[13]:

A = np.random.normal(0,1, (num_items_features,L_rank))


# In[14]:

B = np.random.normal(0,1, (L_rank,(num_users_features+1)))


# In[15]:

L = A.dot(B)


# In[16]:

num_nonzeros_ele = int(num_ele*S_sparsity)


# In[17]:

non_zeros_index_raw = np.array([True]* num_nonzeros_ele +[False] * (num_ele - num_nonzeros_ele))


# In[18]:

np.random.shuffle(non_zeros_index_raw)


# In[19]:

non_zeros_index = np.reshape(non_zeros_index_raw,(num_items_features,(num_users_features+1)))


# In[219]:

S_raw = np.random.normal(0,20,L.shape)


# In[221]:

S = np.where(non_zeros_index, S_raw,0)


# In[205]:

combination_list = itertools.combinations(range(num_items),2)
comb_ind = [list(x) for x in combination_list]


# In[206]:

num_item_pair = len(comb_ind)


# In[207]:

item_pair_feature = np.empty((num_item_pair,num_items_features))


# In[208]:

num_total_feature = num_items_features*(num_users_features+1)


# In[209]:

for pair_id, choice_id in enumerate(comb_ind):
    item_pair_feature[pair_id,:] = item_input[choice_id[0],:] - item_input[choice_id[1],:]


# In[27]:

user_feature = np.concatenate((user_input, np.ones((num_users,1))),axis=1)


# In[28]:

train_x = np.empty((num_item_pair *num_users,num_total_feature))


# In[36]:

num_samples = train_x.shape[0]


# In[30]:

for pair_id in range(len(comb_ind)):
    for user_id in range(num_users):
        train_x[pair_id*num_users+user_id,:] = np.outer(item_pair_feature[pair_id,:],user_feature[user_id,:]).reshape((1,num_total_feature))


# In[222]:

Omega = L+S


# In[223]:

Omega_v = np.reshape(Omega,(num_total_feature,1))


# In[108]:

train_x = preprocessing.scale(train_x)


# In[224]:

prob = train_x.dot(Omega_v)


# In[226]:

train_t = np.empty((num_samples,1))


# In[227]:

for sample_ind in range(num_samples):
    p = 1.0/(1+ math.exp(-prob[sample_ind] + np.random.normal(0,0.1,1)))
    train_t[sample_ind] = np.random.choice([0, 1], size=1, p=[1-p, p])


# In[112]:

train_x_final = np.concatenate((train_x, -train_x),axis = 0)


# In[228]:

train_t_final = np.concatenate((train_t,1-train_t),axis=0).reshape(2*num_samples)


# In[114]:

np.savetxt('train_x.csv',train_x_final,delimiter=',')


# In[229]:

np.savetxt('train_t.csv',train_x_final,delimiter=',')


# In[230]:

np.savetxt('L.csv',L,delimiter=',')


# In[231]:

np.savetxt('S.csv',S,delimiter=',')


# In[233]:

clf = linear_model.LogisticRegression(penalty='l2', dual=False, tol=0.0001, C=0.01, fit_intercept=True,
      intercept_scaling=1, class_weight=None, random_state=None) 


# In[232]:

np.unique(train_t_final)


# In[234]:

clf.fit(train_x_final,train_t_final)


# In[235]:

clf.score(train_x_final,train_t_final)


# In[236]:

stop_rule = 1e-8
max_iter = 5000


# In[321]:

# nuclear norm ---- low rank
lambda1 = 0.01
# l1 norm ---- sparsity
lambda2 = 0.001
# stepsize
t = 0.01


# In[322]:

tol = 10
iter_num = 0


# In[323]:

prev_omega_v = clf.coef_.reshape(train_x_final.shape[1])


# In[324]:

prev_omega = prev_omega_v.reshape((num_items_features,(num_users_features+1)))


# In[325]:

prev_L = prev_omega


# In[326]:

prev_S = np.zeros(prev_L.shape)


# In[172]:


def soft_thresholding(S, lambda2, t):
    '''
    Proximal operator for l1 norm
    '''
    
    prox_S = np.fmax(np.fabs(S) - lambda2 * t, 0) * np.sign(S)
    
    return prox_S

def matrix_shrinkage(L, lambda1, t):
    '''
    Proximal operator for nuclear norm term
    Required a svd function
    '''
    U, Sigma, V = np.linalg.svd(L)
    Sigma_shrink = np.diag(soft_thresholding(Sigma, lambda1, t))
    if U.shape[0] > V.shape[0]:
        Sigma_hat = np.concatenate((Sigma_shrink,np.zeros((U.shape[0] - V.shape[0], V.shape[0]))),axis =0)
    elif U.shape[0] < V.shape[0]:
        Sigma_hat = np.concatenate((Sigma_shrink,np.zeros((U.shape[0], V.shape[0]-U.shape[0]))),axis =1)
    else:
        Sigma_hat =  Sigma_shrink   
    prox_L =  np.dot(np.dot(U,Sigma_hat),V) 
    
    return prox_L
    
    
def L_update(L_k, t, grad_k, lambda1):
    '''
    Update L
    '''
    L_temp = L_k - t * grad_k
    L_new = matrix_shrinkage(L_temp, lambda1, t)
    return L_new
    
    
def S_update(S_k, t, grad_k, lambda2):
    '''
    Update S
    '''
    S_temp = S_k - t * grad_k
    S_new = soft_thresholding(S_temp, lambda2, t)
    return S_new


def gradient(train_x_mini,train_t_mini,omega):
    x = np.dot(train_x_mini, omega) 
    logit_term = logistic.cdf(x)
    res = np.dot(train_x_mini.T, (logit_term-train_t_mini))
    res = res/train_t_mini.shape[0]
    return(res)
    
    
    
def validation(valid_x, valid_t,omega):
    x = np.dot(valid_x, omega)
    predict = np.where(logistic.cdf(x)>0.5,1,0)
    accuracy = np.where(predict == valid_t, 1, 0)
    return(1.0*np.sum(accuracy)/valid_t.shape[0])
    


# In[327]:

while (tol > stop_rule and iter_num < max_iter):
    #print iter_num
    gradient_v = gradient(train_x_final,train_t_final,prev_omega_v)
    gradient_m = gradient_v.reshape((num_items_features,(num_users_features+1)))
    
    new_L = L_update(prev_L, t, gradient_m, lambda1)
    new_S = S_update(prev_S,t, gradient_m,lambda2)
    new_omega = new_L + new_S
        
    #L_diff = np.sum(np.fabs(new_L - prev_L))/np.sum(np.fabs(prev_L))
    #S_diff = np.sum(np.fabs(new_S - prev_S))/np.sum(np.fabs(prev_S))
    tol = np.sum(np.fabs(new_omega - prev_omega))/np.sum(np.fabs(prev_omega))
    iter_num = iter_num + 1
    print tol
    prev_L = new_L
    prev_S = new_S
    prev_omega = new_omega


# In[328]:

np.linalg.matrix_rank(new_L)


# In[329]:

omega_v = prev_omega.reshape(train_x_final.shape[1])
#valid_score = validation(train_x, train_t, omega_v)


# In[330]:

x = np.dot(train_x, omega_v)
predict = np.where(logistic.cdf(x)>0.5,1,0)
accuracy = np.where(predict == train_t.reshape(predict.shape), 1, 0)



# In[317]:

#accuracy = np.where(predict == train_t.reshape(predict.shape), 1, 0)


# In[331]:

1.0*np.sum(accuracy)/accuracy.shape[0]


# In[275]:

np.linalg.matrix_rank(clf.coef_.reshape((num_items_features,(num_users_features+1))))


# In[332]:

new_L


# In[277]:

L


# In[180]:

clf.coef_


# In[333]:

new_S


# In[153]:

S


# In[334]:

L+S


# In[336]:

new_L+new_S


# In[338]:

new_S


# In[341]:

import matplotlib.pyplot as plt



# In[339]:

plot_url = py.plot(L)


# In[346]:

plt.pcolor(S)
plt.show()
plt.pcolor(new_S)
plt.show()


# In[ ]:



