#------------------------------------------------------------------------------
#               Classification with low rank and sparsity structure
#               Author: Yanxin Pan
#               Date: Jan. 5, 2015
#               Notes: 
#------------------------------------------------------------------------------


import time
import os

import getopt
import sys
import numpy as np
from sklearn import linear_model, cross_validation, preprocessing
from numpy.random import RandomState
from sklearn.externals import joblib
from minibatch_helpers import create_random_choice_data, create_minibatch, minibatcher, train_classifier
from low_sparse_approximation import gradient, validation, S_update, L_update, soft_thresholding, matrix_shrinkage
from scipy.stats import logistic
import pdb
from random import shuffle



def make_choice(ind,order):
    # iterate through pairs of items twice
    num_item = order.shape[1]

    ind = np.array(ind)
    num_user = ind.shape[0]

    choice_data = np.zeros([ind.shape[0]*num_item*(num_item-1),4])
    counter = 0

    for i in range(num_item):
        for j in range(num_item):
            if not i==j:
                choice_data[counter*num_user:(counter+1)*num_user,0] = ind
                choice_data[counter*num_user:(counter+1)*num_user,1] = i*np.ones(num_user)
                choice_data[counter*num_user:(counter+1)*num_user,2] = j*np.ones(num_user)
                counter += 1

    for i in range(choice_data.shape[0]):
        # if order[user_id,item1_id]<order[user_id,item2_id] set choice_data[i,3] to 1
        if find(choice_data[i,1],order[choice_data[i,0],:]) < find(choice_data[i,2],order[choice_data[i,0],:]):
            choice_data[i,3] = 1

    return choice_data

def find(x,arr):
    for ind, item in enumerate(arr):
        if item == x:
            return ind



################################################################################################################



# nuclear norm ---- low rank
lambda1 = 8
# l1 norm ---- sparsity
lambda2 = 1
# stepsize
t = 0.0005


make_new_data = False

#test_size = 100000
batch_size = 225000

stop_rule = 0.0001
max_iter = 5000

cv_test_ratio = 0.75
train_val_ratio = 2.0/3

try:
    opts, args=getopt.getopt(sys.argv[1:],"hl:L:t:b:s:e:",["lambda1=","lambda2=","step_size=","batch_size=","stop_rule=","max_iter="])
except getopt.GetoptError:
    print 'gae_demo.py -i <inputfile>'
    sys.exit(2)

for opt, arg in opts:
    if opt =='-h':
        print 'classification_lsaw.py -l1 <lambda1> -l2 <lambda2> -t <step_size> -T <test_size> -b <batch_size> -s <stop_rule> -e <max_iter>'
    elif opt in("-l", "--lambda1="):
        lambda1 = float(arg)
    elif opt in("-L", "--lambda2="):
        lambda2 = float(arg)
    elif opt in("-t","--step_size="):
        t = float(arg)
    #elif opt in("-T","--test_size="):
        #test_size = int(arg)
    elif opt in("-b","--batch_size="):
        batch_size = int(arg)
    elif opt in("-s","--stop_rule="):
        stop_rule = float(arg)
    elif opt in("-e","--max_iter="):
        max_iter = int(arg)
    #elif opt in("-r","--rand_seed="):
        #rand_seed = int(arg)

seed_vector = range(1,6)
indices_dir = "../data/indices"+str(len(seed_vector))+"/"
results_clf_dir = "../results/attributes_mturk/best_clf_attributes/"
results_dir = "../results/attributes_mturk/"
#num_experiments = 3
accuracies = np.zeros(len(seed_vector))
baseline_accuracies = np.zeros(len(seed_vector))

user_variables = np.load("../data/sushi/sushi3_udata_m_hot.npy")
#item_variables = np.genfromtxt("../data/sushi/sushi3.idata", delimiter=' ')
item_variables = np.load("../data/sushi/sushi3_idata_m_hot.npy")
item_variables = item_variables[:10,:]
#design_variables_mask  = np.loadtxt("../data/processed_data/mturk_labeled/mask_layer_subtract_JAG.csv", delimiter=',')
#design_attributes = np.loadtxt("../data/processed_data/mturk_labeled_12_17_14/mturk_labeled_10attributes.csv", delimiter=',')
order = np.loadtxt("../data/sushi/sushi3a.5000.10.order")
order = order[:,2:]

num_user = user_variables.shape[0]
num_item = item_variables.shape[0]

num_users_vars = user_variables.shape[1]
num_items_vars = item_variables.shape[1]

#initialize the L and S
prev_L = np.zeros(((num_users_vars+1), num_items_vars))
prev_S = np.zeros(((num_users_vars+1), num_items_vars))
prev_omega = prev_L


#------------------ Check Dirs ----------------------------------
if not os.path.exists( results_clf_dir ):
    os.makedirs( results_clf_dir )
    
if not os.path.exists( indices_dir):
    os.makedirs(indices_dir)

for exp_num, seed in enumerate(seed_vector):

    #train_choice_data = np.loadtxt(indices_dir+"train_choice_seed_%s_ALLchoice.csv" % seed,  delimiter=',', dtype=int)
    #valid_choice_data = np.loadtxt(indices_dir+"valid_choice_seed_%s_ALLchoice.csv" % seed, delimiter=',', dtype=int)
    #test_choice_data = np.loadtxt(indices_dir+"test_choice_seed_%s_ALLchoice.csv" % seed, delimiter=',', dtype=int)

    print "Using seed %d" % seed
    choice_file = indices_dir+"choice_file"+str(seed)+".npy"
    if not os.path.exists(choice_file):
        train_ind = range(num_user)
        shuffle(train_ind)
        test_ind = train_ind[int(cv_test_ratio*len(train_ind)+1):]
        train_ind = train_ind[:int(cv_test_ratio*len(train_ind))]
        val_ind = train_ind[int(train_val_ratio*len(train_ind)+1):]
        train_ind = train_ind[:int(train_val_ratio*len(train_ind))]
        train_choice_data = make_choice(train_ind,order)
        valid_choice_data = make_choice(val_ind,order)
        test_choice_data = make_choice(test_ind,order)
        np.save(choice_file,[train_choice_data,valid_choice_data,test_choice_data])
    else:
        choice_dict = np.load(choice_file)
        train_choice_data = choice_dict[0]
        valid_choice_data = choice_dict[1]
        test_choice_data = choice_dict[2]


    num_batches = train_choice_data.shape[0]/batch_size

    #minibatcher_valid_instance = minibatcher(valid_choice_data, batch_size, design_features, user_features)
    counter = 1
    #------------------- Begin Minibatch Training ---------------------------------------------------
    #------------------ Get Variables and Features -------------------------
    unique_user_inds = np.unique(train_choice_data[:,[0]])
    unique_design_inds = np.unique(train_choice_data[:,[1,2]])
    scaler_users = preprocessing.StandardScaler()
    scaler_designs = preprocessing.StandardScaler()            
    scaler_users.fit(user_variables[unique_user_inds.astype(int), :])
    scaler_designs.fit(item_variables[ unique_design_inds.astype(int), :])
    user_features = scaler_users.transform(user_variables)
    design_features = scaler_designs.transform(item_variables)
    #design_features[~np.isnan(design_variables[:, 32]), :] = scaler_designs.transform(design_variables[~np.isnan(design_variables[:, 32]), :])
    #design_features = scaler_designs.transform(design_variables[np.isnan(design_variables[:, 32]), :])
    print "------Loaded all data..."

    #------------------ Create TRAINING Data ----------------------------------
    train_x_mini,train_t_mini = create_minibatch( train_choice_data, design_features, user_features)
    #------------------ Create Validation Data ----------------------------------
    valid_x, valid_t = create_minibatch( valid_choice_data, design_features, user_features )
    #------------------ Create Classifiers ------------------------


    classifiers = [linear_model.LogisticRegression(penalty='l2', dual=False, tol=0.0001, C=0.01, fit_intercept=True,
          intercept_scaling=1, class_weight=None, random_state=None) ]

    minibatcher_instance = minibatcher(train_choice_data, batch_size, design_features, user_features) 
    for batch_num, (train_x_mini, train_t_mini) in enumerate(minibatcher_instance):
        print "On batch %s of %s" % (batch_num, num_batches)
        while (batch_num == 0):
            tol = 10
            iter_num = 0
            
            for clf_num, clf in enumerate(classifiers):
                print 'Training baseline classifier'
                clf.fit(train_x_mini, train_t_mini)
                valid_s = clf.score(valid_x, valid_t)
                baseline_accuracies[exp_num] = valid_s
                print "Baseline accuracy: %4.2f." % valid_s
                
                baseline_accuracies[exp_num] = valid_s
                prev_omega_v = clf.coef_.reshape(train_x_mini.shape[1])
                
                prev_omega = prev_omega_v.reshape(((num_users_vars+1), num_items_vars))
                prev_L = prev_omega
                
                print "prev_L rank: %4.2f" % np.linalg.matrix_rank(prev_L)
            
            print 'Training lsa classifier'
            while (tol > stop_rule and iter_num < max_iter):
                
               
               #prev_omega_v = prev_omega.reshape(train_x_mini.shape[1])
               gradient_v = gradient(train_x_mini,train_t_mini,prev_omega_v)
               gradient_m = gradient_v.reshape(((num_users_vars+1), num_items_vars))
        
               new_L = L_update(prev_L, t, gradient_m, lambda1)
               new_S = S_update(prev_S,t, gradient_m,lambda2)
               new_omega = new_L + new_S
        
               #L_diff = np.sum(np.fabs(new_L - prev_L))/np.sum(np.fabs(prev_L))
               #S_diff = np.sum(np.fabs(new_S - prev_S))/np.sum(np.fabs(prev_S))
               tol = np.sum(np.fabs(new_omega - prev_omega))/np.sum(np.fabs(prev_omega))
               iter_num = iter_num + 1
         
               prev_L = new_L
               prev_S = new_S
               prev_omega = new_omega
               
               if iter_num%500 ==0:
                   print "iter_num: %d" % iter_num
                   print "tol: %4.6f" % tol

            break



    omega_v = prev_omega.reshape(train_x_mini.shape[1])
    valid_score = validation(valid_x, valid_t, omega_v)
    accuracies[exp_num] = valid_score
    print valid_score
    print np.linalg.matrix_rank(new_L)

print 'baseline model'
print np.mean(baseline_accuracies) 
print np.std(baseline_accuracies)
print 'lsa model'
print np.mean(accuracies) 
print np.std(accuracies)

