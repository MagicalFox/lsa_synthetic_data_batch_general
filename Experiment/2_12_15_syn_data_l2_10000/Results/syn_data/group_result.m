clc
clear
close all

load('cent_results.csv')
data = cent_results;
lam1_list = unique(data(:,1));
lam2_list = unique(data(:,2));
results = zeros(length(lam1_list),length(lam2_list),10);

for i =1:length(lam1_list)
    for j = 1:length(lam2_list)
        grid_results = data(data(:,1)==lam1_list(i),:);
        grid_results = grid_results(grid_results(:,2)==lam2_list(j),:);
        results(i,j,:) = grid_results(:,6);
    end
end
