from os import listdir
from os.path import isfile, join
import numpy as np
import pdb
mypath = '../Results/syn_data/'
result_files = [ f for f in listdir(mypath) if isfile(join(mypath,f))]
cent_results_file = mypath+'cent_results.csv'
all_results = np.zeros([len(result_files),8])
result_count = 0
for filename in result_files:
    name_chunks = filename.split('_')
    if len(name_chunks)>5 and name_chunks[1] == 'seed':
        data = np.loadtxt(mypath+filename,delimiter=',')
        # features needed: C, l1/l2, acci[0-4]
        try:
            lambda1 = float(name_chunks[3])
            lambda2 = float(name_chunks[5])
            seed_num = int(name_chunks[2].split('lambda1')[0])
	    all_results[result_count,0] = lambda1
            all_results[result_count,1] = lambda2
            all_results[result_count,2] = seed_num
            all_results[result_count,3:8] = data
            result_count+=1
        except:
            print 'File: ' + filename + ' is not a LSA result file'


all_results = all_results[:result_count,:]
np.savetxt(cent_results_file,all_results,delimiter=',')
