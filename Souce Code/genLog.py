from os import listdir
from os.path import isfile, join
import numpy as np
import pdb
mypath = '../Results/'
result_files = [ f for f in listdir(mypath) if isfile(join(mypath,f))]
pdb.set_trace()
for filename in result_files:
    name_chunks = filename.split('_')
    if len(name_chunks)>5 and name_chunks[2][:3] == 'lam':
        # features needed: C, l1/l2, acc[0-4]
        try:
            lambda1 = float(name_chunks[3])
            lambda2 = float(name_chunks[5])
            step_size = float(name_chunks[-1].replace('.csv',''))
            max_iter = int(name_chunks[7])
            log_name = 'classification_sushi_multiple_experiment_notol_10000iter_data_sweep_lam1_'+str(lambda1)+'_lam2_'+str(lambda2)+'_t_'+str(step_size)+'_max_iter_'+str(max_iter)+'.finished.npy'
            open('../Results/Log/'+log_name,'w')
        except:
            print 'File: ' + filename + ' is not a LSA result file'


