import getopt
import numpy as np
import sys
import random
import itertools
import math
import os.path
import matplotlib.pyplot as plt
from sklearn import linear_model, cross_validation, preprocessing
from scipy.stats import logistic
from os.path import isfile

import pdb


'''
try:
    opts, args=getopt.getopt(sys.argv[1:], "hl:L:t:s:e:",
            ["lambda1=","lambda2=","step_size=","batch_size=","stop_rule=","max_iter=","baseline_c=","Baseline_only=","regularizer="])
except getopt.GetoptError:
    print 'gae_demo.py -i <inputfile>'
    sys.exit(2)

for opt, arg in opts:
    if opt =='-h':
        print 'classification_lsaw.py -l1 <lambda1> -l2 <lambda2> -t <step_size> -T <test_size> -b <batch_size> -s <stop_rule> -e <max_iter> -r <regularizer>'
    elif opt in("-l", "--lambda1="):
        lambda1 = float(arg)
    elif opt in("-L", "--lambda2="):
        lambda2 = float(arg)
    elif opt in("-t","--step_size="):
        t = float(arg)
    elif opt in("-s","--stop_rule="):
        stop_rule = float(arg)
    elif opt in("-e","--max_iter="):
        max_iter = int(arg)
'''

m=10
n=10
S_sparsity = 0.05
t=0.1
p = 2


def matrix_shrinkage(L, lambda1, t):
    '''
    Proximal operator for nuclear norm term
    Required a svd function
    '''
    U, Sigma, V = np.linalg.svd(L)
    Sigma_shrink = np.diag(soft_thresholding(Sigma, lambda1, t))
    if U.shape[0] > V.shape[0]:
        Sigma_hat = np.concatenate((Sigma_shrink,np.zeros((U.shape[0] - V.shape[0], V.shape[0]))),axis =0)
    elif U.shape[0] < V.shape[0]:
        Sigma_hat = np.concatenate((Sigma_shrink,np.zeros((U.shape[0], V.shape[0]-U.shape[0]))),axis =1)
    else:
        Sigma_hat =  Sigma_shrink   
    prox_L =  np.dot(np.dot(U,Sigma_hat),V) 
    return prox_L
    
    
def L_update(L_k, t, grad_k, lambda1):
    '''
    Update L
    '''
    L_temp = L_k - t * grad_k
    L_new = matrix_shrinkage(L_temp, lambda1, t)
    return L_new
    
    
def S_update(S_k, t, grad_k, lambda2):
    '''
    Update S
    '''
    S_temp = S_k - t * grad_k
    S_new = soft_thresholding(S_temp, lambda2, t)
    return S_new

def soft_thresholding(S, lambda2, t):
    '''
    Proximal operator for l1 norm
    '''    
    prox_S = np.fmax(np.fabs(S) - lambda2 * t, 0) * np.sign(S)
    return prox_S

def gradient(X,L,S,p):
    grad = X-L-S
    return -2*grad/m/n

def loss(X,L,S,p):
    loss = X-L-S
    return np.sum(loss**p)/m/n


def main(parameters):
    
    lambda1 = parameters[0]
    lambda2 = parameters[1]
    max_iter = parameters[2]
    stop_rule = parameters[3]
    seed_i = parameters[4]
    L_rank = parameters[5]

    random.seed(seed_i)
    A = np.random.normal(0,1, (m,L_rank))
    B = np.random.normal(0,1, (L_rank,n))
    L = A.dot(B)

    num_ele = m*n
    num_nonzeros_ele = int(m*n*S_sparsity)
    non_zeros_index_raw = np.array([True]* num_nonzeros_ele +[False] * (num_ele - num_nonzeros_ele))
    np.random.shuffle(non_zeros_index_raw)
    non_zeros_index = np.reshape(non_zeros_index_raw,(m,n))
    S_raw = np.random.uniform(-20,20,L.shape)
    S = np.where(non_zeros_index, S_raw,0)

    X = L + S
    
    result_dir = '../Results/syn_data_rank_'+str(L_rank)+'/'
    if not os.path.isdir(result_dir):
        os.makedirs(result_dir)
    
    prev_L = np.zeros(X.shape)
    prev_S = np.zeros(X.shape)


    iter_num = 0
    tol = 10000
    while (tol > stop_rule and iter_num < max_iter):
        
        try:
            gradient_m = gradient(X, prev_L, prev_S,p)
        except:
            pdb.set_trace()

        new_L = L_update(prev_L, t, gradient_m, lambda1)
        new_S = S_update(prev_S,t, gradient_m,lambda2)

        iter_num =iter_num+1
        prev_L = new_L
        prev_S = new_S




        if iter_num%1000==0:
            print iter_num

    L_diff = L-new_L
    L_diff_2n = np.linalg.norm(L_diff)
    S_diff = S-new_S
    S_diff_2n = np.linalg.norm(S_diff)
    grad_2n = np.linalg.norm(gradient_m)

    Rank = np.linalg.matrix_rank(new_L)
    sparsity = float(len(new_S.nonzero()[0]))/new_S.shape[0]/new_S.shape[1]
    print 'L_diff_2n: '+str(L_diff_2n)+'\nS_diff_2n: '+str(S_diff_2n)+'\ngrad_2n: '+str(grad_2n)+'\nRank: '+str(Rank)+'\nSparsity: '+str(sparsity)
    
    result_file_name =result_dir+'syntheticDataResult_'+'seed_'+str(seed_i)+'lambda1_'+str(lambda1)+'_lambda2_'+str(lambda2)+'_max-iter_'+str(max_iter)+'_.csv'
    np.savetxt(result_file_name,[L_diff_2n,S_diff_2n,grad_2n,Rank,sparsity],delimiter=',')

    L_file_name = result_dir+'syntheticDataResult_L_'+'seed_'+str(seed_i)+'lambda1_'+str(lambda1)+'_lambda2_'+str(lambda2)+'_max-iter_'+str(max_iter)+'.csv'
    np.savetxt(L_file_name,L,delimiter=',')

    S_file_name = result_dir+'syntheticDataResult_S_'+'seed_'+str(seed_i)+'lambda1_'+str(lambda1)+'_lambda2_'+str(lambda2)+'_max-iter_'+str(max_iter)+'.csv'
    np.savetxt(S_file_name,S,delimiter=',')

    new_L_file_name = result_dir+'syntheticDataResult_new_L_'+'seed_'+str(seed_i)+'lambda1_'+str(lambda1)+'_lambda2_'+str(lambda2)+'_max-iter_'+str(max_iter)+'.csv'
    np.savetxt(new_L_file_name,L,delimiter=',')

    new_S_file_name = result_dir+'syntheticDataResult_new_S_'+'seed_'+str(seed_i)+'lambda1_'+str(lambda1)+'_lambda2_'+str(lambda2)+'_max-iter_'+str(max_iter)+'.csv'
    np.savetxt(new_S_file_name,S,delimiter=',')




